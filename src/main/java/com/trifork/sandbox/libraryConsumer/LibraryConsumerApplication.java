package com.trifork.sandbox.libraryConsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LibraryConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(LibraryConsumerApplication.class, args);
	}

}
